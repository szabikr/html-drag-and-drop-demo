import Layout from "./components/Layout";
import CopyOperation from "./pages/CopyOperation";
import DefaultDragAndDrop from "./pages/DefaultDragAndDrop";
import DropzonesAndFiles from "./pages/DropzonesAndFiles";
import Home from "./pages/Home";
import LinkOperation from "./pages/LinkOperation";
import MoveBetweenDropzones from "./pages/MoveBetweenDropzones";
import MoveOperation from "./pages/MoveOperation";
import Practice from "./pages/Practice";

export default function App() {
  const currentPath = window.location.pathname.slice(1) as PathType;

  const paths = {
    [path.HOME]: <Home />,
    [path.DEFAULT_DRAG_AND_DROP]: <DefaultDragAndDrop />,
    [path.DROPZONES_AND_FILES]: <DropzonesAndFiles />,
    [path.MOVE_OPERATION]: <MoveOperation />,
    [path.MOVE_BETWEEN_DROPZONES]: <MoveBetweenDropzones />,
    [path.COPY_OPERATION]: <CopyOperation />,
    [path.LINK_OPERATION]: <LinkOperation />,
    [path.PRACTICE]: <Practice />,
  };

  const page = paths[currentPath];

  return (
    <Layout currentPath={currentPath}>{page ? page : paths[path.HOME]}</Layout>
  );
}

// ======================
//    # Data & Types #
// ======================

const path = {
  HOME: "",
  DEFAULT_DRAG_AND_DROP: "default-drag-and-drop",
  DROPZONES_AND_FILES: "dropzones-and-files",
  MOVE_OPERATION: "move-operation",
  MOVE_BETWEEN_DROPZONES: "move-between-dropzones",
  COPY_OPERATION: "copy-operation",
  LINK_OPERATION: "link-operation",
  PRACTICE: "practice",
} as const;

type PathType = (typeof path)[keyof typeof path];
