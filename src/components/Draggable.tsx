import { PropsWithChildren, useState, DragEvent } from "react";
import styled from "styled-components";

interface DraggableProps {
  onDragStart: (event: DragEvent<HTMLDivElement>) => void;
  onDragEnd?: (event: DragEvent<HTMLDivElement>) => void;

  // Should find a better way to define forwarding prop types
  [propName: string]: unknown;
}

export default function Draggable({
  children,
  onDragStart,
  onDragEnd,
  ...props
}: PropsWithChildren<DraggableProps>) {
  const [isDragging, setIsDragging] = useState(false);

  const handleDragStart = (event: DragEvent<HTMLDivElement>) => {
    setIsDragging(true);
    onDragStart(event);
  };

  const handleDragEnd = (event: DragEvent<HTMLDivElement>) => {
    if (onDragEnd) onDragEnd(event);
    setIsDragging(false);
  };

  return (
    <DivStyled
      $isDragging={isDragging}
      draggable="true"
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      {...props}
    >
      {children}
    </DivStyled>
  );
}

// ===========================
//    # Styled Components #
// ===========================

const DivStyled = styled.div<{ $isDragging: boolean }>`
  display: inline-block;
  user-select: none;
  background-color: #c2c1a5;
  padding: 1.2em 2em;
  margin: 0.4em 1.2em;
  border: ${(props) => `3px ${props.$isDragging ? "dashed" : "solid"} #ba5c12`};
  border-radius: 8px;
`;
