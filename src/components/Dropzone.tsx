import { useState, DragEvent, PropsWithChildren } from "react";
import styled from "styled-components";

interface DropzoneProps {
  onDragEnter?: (event: DragEvent<HTMLDivElement>) => void;
  onDragOver?: (event: DragEvent<HTMLDivElement>) => void;
  onDrop: (event: DragEvent<HTMLDivElement>) => void;
  onDragLeave?: (event: DragEvent<HTMLDivElement>) => void;

  // Should find a better way to define forwarding prop types
  [propName: string]: unknown;
}

export default function Dropzone({
  children,
  onDragEnter,
  onDragOver,
  onDrop,
  onDragLeave,
  ...props
}: PropsWithChildren<DropzoneProps>) {
  const [hasDragEntered, setHasDragEntered] = useState(false);

  const handleDragOver = (event: DragEvent<HTMLDivElement>) => {
    // Prevent default to allow drop
    event.preventDefault();
    if (onDragOver) onDragOver(event);
  };

  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    onDrop(event);
    setHasDragEntered(false);
  };

  const handleDragEnter = (event: DragEvent<HTMLDivElement>) => {
    setHasDragEntered(true);
    if (onDragEnter) onDragEnter(event);
  };

  const handleDragLeave = (event: DragEvent<HTMLDivElement>) => {
    if (onDragLeave) onDragLeave(event);
    setHasDragEntered(false);
  };

  return (
    <DivStyled
      $dropping={hasDragEntered}
      onDragOver={handleDragOver}
      onDrop={handleDrop}
      onDragEnter={handleDragEnter}
      onDragLeave={handleDragLeave}
      {...props}
    >
      {children}
    </DivStyled>
  );
}

// ===========================
//    # Styled Components #
// ===========================

const DivStyled = styled.div<{ $dropping: boolean }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 400px;
  height: 200px;
  border: 1px solid white;
  border-radius: 8px;
  padding: 0.8em 1em;
  margin: auto;

  background-color: ${(props) =>
    props.$dropping ? "darkslategrey" : "darkslateblue"};
`;
