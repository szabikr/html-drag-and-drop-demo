import { PropsWithChildren } from "react";
import styled from "styled-components";

interface LayoutProps {
  currentPath: string;
}

export default function Layout({
  children,
  currentPath,
}: PropsWithChildren<LayoutProps>) {
  const currentPathIndex = navigationList.findIndex((p) => p === currentPath);

  return (
    <>
      <main>{children}</main>
      <footer>
        <FlexNav>
          <LinkStyled
            href={navigationList[currentPathIndex - 1]}
            $isVisible={currentPathIndex >= 0 && currentPathIndex !== 0}
          >
            {"👈"}
          </LinkStyled>
          <LinkStyled href="/" $isVisible={currentPathIndex >= 0}>
            {"🏠"}
          </LinkStyled>
          <LinkStyled
            href={navigationList[currentPathIndex + 1]}
            $isVisible={
              currentPathIndex >= 0 &&
              currentPathIndex !== navigationList.length - 1
            }
          >
            {" 👉"}
          </LinkStyled>
        </FlexNav>
      </footer>
    </>
  );
}

// ======================
//    # Data & Types #
// ======================

const navigationList = [
  "default-drag-and-drop",
  "dropzones-and-files",
  "move-operation",
  "move-between-dropzones",
  "copy-operation",
  "link-operation",
  "practice",
];

// ===========================
//    # Styled Components #
// ===========================

const FlexNav = styled.nav`
  display: flex;
  justify-content: space-between;
`;

const LinkStyled = styled.a<{ $isVisible: boolean }>`
  display: inline-block;
  font-size: 3em;
  text-decoration: underline;
  visibility: ${(props) => `${props.$isVisible ? "visible" : "hidden"}`};
`;
