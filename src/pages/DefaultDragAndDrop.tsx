import styled from "styled-components";
import Card from "../components/Card";

export default function DefaultDragAndDrop() {
  return (
    <>
      <h1>Default Drag and Drop</h1>
      <FlexBox>
        <Card>
          <h2>Draggable Elements</h2>

          <H4Styled>1. Selected Text</H4Styled>
          <p>The quick brown fox jumps over the lazy dog.</p>

          <H4Styled>2. Hyperlinks</H4Styled>
          <p>
            <a href="https://www.airportlabs.com/">AirportLabs Website</a>
          </p>

          <H4Styled>3. Image</H4Styled>
          <ImgStyled
            alt="too busy to imporove illustration"
            src="https://pbs.twimg.com/media/EiB7GkOVoAAsQFh.png"
          />
        </Card>

        <Card>
          <h2>Dropzones</h2>

          <H4Styled>1. Input type text</H4Styled>
          <InputStyled type="text" />

          <H4Styled>2. Textarea</H4Styled>
          <TextareaStyled rows={10}></TextareaStyled>
        </Card>
      </FlexBox>
    </>
  );
}

// ===========================
//    # Styled Components #
// ===========================

const FlexBox = styled.div`
  display: flex;
  justify-content: space-between;
  text-align: left;
`;

const ImgStyled = styled.img`
  max-width: min(80%, 600px);
  height: auto;
`;

const H4Styled = styled.h4`
  margin-top: 48px;
`;

const InputStyled = styled.input`
  padding: 8px 12px;
  width: 380px;
`;

const TextareaStyled = styled.textarea`
  padding: 8px 12px;
  width: 380px;
`;
