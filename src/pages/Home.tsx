import styled from "styled-components";

export default function Home() {
  return (
    <>
      <h1>HTML5 Drag and Drop Demo</h1>
      <FlexNav>
        <LinkStyled href="/default-drag-and-drop">
          Default drag and drop
        </LinkStyled>
        <LinkStyled href="/dropzones-and-files">Dropzones and files</LinkStyled>
        <LinkStyled href="/move-operation">Move operation</LinkStyled>
        <LinkStyled href="/move-between-dropzones">
          Move between dropzones
        </LinkStyled>
        <LinkStyled href="/copy-operation">Copy operation</LinkStyled>
        <LinkStyled href="/link-operation">Link operation</LinkStyled>
        <LinkStyled href="/practice">Practice</LinkStyled>
      </FlexNav>
    </>
  );
}

// ===========================
//    # Styled Components #
// ===========================

const FlexNav = styled.nav`
  margin-top: 8em;
  text-align: center;
`;

const LinkStyled = styled.a`
  display: block;
  margin-top: 1.2em;
  font-size: 1.8em;
  text-decoration: underline;
`;
