import { DragEvent } from "react";
import Card from "../components/Card";
import Dropzone from "../components/Dropzone";

export default function DropzonesAndFiles() {
  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    // Prevent file from being opened by the browser
    event.preventDefault();
    [...event.dataTransfer.files].map((file: File, index) => {
      console.log(`file #${index}: ${file.name}`);
    });
  };

  return (
    <>
      <h1>Dropzones and Files</h1>
      <Card>
        <h2>Dropzone</h2>
        <Dropzone onDrop={handleDrop} />
      </Card>
    </>
  );
}
