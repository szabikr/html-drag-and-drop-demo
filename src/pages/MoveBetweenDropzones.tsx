import { DragEvent, useState } from "react";
import { v4 as uuid } from "uuid";
import styled from "styled-components";
import Card from "../components/Card";
import Draggable from "../components/Draggable";
import Dropzone from "../components/Dropzone";

export default function MoveBetweenDropzones() {
  const [tasks, setTasks] = useState<TaskData[]>([]);
  const [taskName, setTaskName] = useState<string>("");

  const handleNewTaskClick = () => {
    const id = uuid().substring(0, 8);
    setTasks([
      ...tasks,
      { id, name: `task ${taskName === "" ? id : taskName}` },
    ]);
    setTaskName("");
  };

  const handleDragStart = (event: DragEvent<HTMLDivElement>) => {
    event.dataTransfer.setData("text/plain", event.currentTarget.id);
  };

  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    const elementId = event.dataTransfer.getData("text/plain");
    const element = document.getElementById(elementId);
    if (!element) return;
    event.currentTarget.appendChild(element);
  };

  return (
    <>
      <h1>Move Between Dropzones</h1>
      <FlexBox>
        <Card>
          <h2>Todo</h2>
          <Dropzone onDrop={handleDrop}>
            {tasks.map((t) => (
              <Draggable key={t.id} id={t.id} onDragStart={handleDragStart}>
                <SpanStyled>{t.name}</SpanStyled>
              </Draggable>
            ))}
          </Dropzone>
          <DivStyled>
            <InputStyled
              placeholder="Task name..."
              value={taskName}
              onChange={(e) => setTaskName(e.target.value)}
            />
            <button onClick={handleNewTaskClick}>New Task</button>
          </DivStyled>
        </Card>

        <Card>
          <h2>Done</h2>
          <Dropzone onDrop={handleDrop} />
        </Card>
      </FlexBox>
    </>
  );
}

// ======================
//    # Data & Types #
// ======================

type TaskData = {
  id: string;
  name: string;
};

// ===========================
//    # Styled Components #
// ===========================

const FlexBox = styled.div`
  display: flex;
  justify-content: space-around;
`;

const DivStyled = styled.div`
  margin-top: 1em;
`;

const InputStyled = styled.input`
  padding: 0.8em 1em;
  margin-right: 0.8em;
  background-color: #dfe0dc;
  color: #351431;
  border: none;
  border-radius: 8px;
  font-size: 1.1rem;
  border: 2px solid transparent;
  outline: none;
  width: 220px;
  &:focus {
    border-color: #e08dac;
  }
`;

const SpanStyled = styled.span`
  color: #ba5c12;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 0.9rem;
`;
