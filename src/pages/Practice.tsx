import { DragEvent, useState } from "react";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { v4 as uuid } from "uuid";
import styled from "styled-components";
import Card from "../components/Card";
import Dropzone from "../components/Dropzone";
import Draggable from "../components/Draggable";

export default function Practice() {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [content, setContent] = useState<ElementData | null>(null);

  const handleDragStart = (event: DragEvent<HTMLDivElement>, value: string) => {
    console.log(`drag starts with event: ${event} and value: ${value}`);
  };

  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    console.log("drop with event", event);
  };

  return (
    <>
      <h1>Link Operation</h1>
      <Card>
        <Dropzone onDrop={handleDrop}>
          <DroppedElement>{content?.value}</DroppedElement>
          <DivStyled>
            <code>{content?.id}</code>
          </DivStyled>
        </Dropzone>
      </Card>
      <Card>
        {elements.map((e) => (
          <Draggable
            key={e.id}
            onDragStart={(event) => handleDragStart(event, e.value)}
          >
            <SpanStyled>{e.name}</SpanStyled>
          </Draggable>
        ))}
      </Card>
    </>
  );
}

// ======================
//    # Data & Types #
// ======================

type ElementData = {
  id: string;
  value: string;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const CONTRACT = "dnd-link";

const elements = [
  {
    id: "7afad19c",
    name: "Element #1",
    value: "🥦",
  },
  {
    id: "e8dd6480",
    name: "Element #2",
    value: "💰",
  },
  {
    id: "081c67fc",
    name: "Element #3",
    value: "🪵",
  },
];

// ===========================
//    # Styled Components #
// ===========================

const DroppedElement = styled.span`
  font-size: 4.2em;
`;

const DivStyled = styled.div`
  margin-top: 3.3em;
  text-transform: uppercase;
  font-weight: bold;
`;

const SpanStyled = styled.span`
  color: #ba5c12;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 0.9rem;
`;
