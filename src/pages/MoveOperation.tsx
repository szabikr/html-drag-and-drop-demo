import { DragEvent } from "react";
import styled from "styled-components";
import Card from "../components/Card";
import Dropzone from "../components/Dropzone";
import Draggable from "../components/Draggable";

export default function MoveOperation() {
  const handleDragStart = (event: DragEvent<HTMLDivElement>) => {
    console.log("drag has started");
    event.dataTransfer.setData("text/plain", event.currentTarget.id);
  };

  const handleDrag = () => {
    console.log("is dragging");
  };

  const handleDragEnd = () => {
    console.log("drag has ended");
  };

  const handleDragEnter = () => {
    console.log("drag has entered");
  };

  const handleDragOver = () => {
    // console.log("is dragging over");
  };

  const handleDragLeave = () => {
    console.log("drag has left");
  };

  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    const elementId = event.dataTransfer.getData("text/plain");
    const element = document.getElementById(elementId);
    if (!element) return;
    event.currentTarget.appendChild(element);
    console.log("has dropped");
  };

  return (
    <>
      <h1>Move Operation</h1>
      <FlexBox>
        <Card>
          <h2>Draggable elements</h2>
          <Draggable
            id="element-id"
            onDragStart={handleDragStart}
            onDrag={handleDrag}
            onDragEnd={handleDragEnd}
          >
            <SpanStyled>Element</SpanStyled>
          </Draggable>
        </Card>

        <Card>
          <h2>Dropzone</h2>
          <Dropzone
            onDragEnter={handleDragEnter}
            onDragOver={handleDragOver}
            onDrop={handleDrop}
            onDragLeave={handleDragLeave}
          />
        </Card>
      </FlexBox>
    </>
  );
}

// ===========================
//    # Styled Components #
// ===========================

const FlexBox = styled.div`
  display: flex;
  justify-content: space-around;
`;

const SpanStyled = styled.span`
  color: #ba5c12;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 0.9rem;
`;
