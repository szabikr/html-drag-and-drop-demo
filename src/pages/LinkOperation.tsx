import { DragEvent, useState } from "react";
import { v4 as uuid } from "uuid";
import styled from "styled-components";
import Card from "../components/Card";
import Dropzone from "../components/Dropzone";
import Draggable from "../components/Draggable";

export default function LinkOperation() {
  const [content, setContent] = useState<ElementData | null>(null);
  const [level, setLevel] = useState<number>(1);

  const handleDragStart = (
    event: DragEvent<HTMLDivElement>,
    value: string,
    contract: string
  ) => {
    const elementData: ElementData = { id: uuid().substring(0, 8), value };
    event.dataTransfer.setData(contract, JSON.stringify(elementData));
  };

  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    const dataTransfer = event.dataTransfer;

    function getData(contract: string): ElementData | null {
      if (!dataTransfer.types.includes(contract)) {
        console.log(`Only drop elements that conform to ${contract} contract`);
        return null;
      }

      try {
        return JSON.parse(event.dataTransfer.getData(contract));
      } catch (error: unknown) {
        console.log("Incorrect data format in dataTransfer object");
        return null;
      }
    }

    let data = getData(LEVEL_1_CONTRACT);

    if (data === null && level === 2) {
      data = getData(LEVEL_2_CONTRACT);
    }

    if (data === null) {
      return;
    }

    if (data.value === "💰") {
      setLevel(2);
    }

    setContent(data);
  };

  return (
    <>
      <h1>Link Operation</h1>
      <Card>
        <Dropzone onDrop={handleDrop}>
          <DroppedElement>{content?.value}</DroppedElement>
          <DivStyled>
            <code>{content?.id}</code>
          </DivStyled>
        </Dropzone>
      </Card>
      <Card>
        <h2>Level 1</h2>
        {elements.map((e) => (
          <Draggable
            key={e.id}
            onDragStart={(event) =>
              handleDragStart(event, e.value, LEVEL_1_CONTRACT)
            }
          >
            <SpanStyled $isFaded={false}>{e.name}</SpanStyled>
          </Draggable>
        ))}
      </Card>
      <Card>
        <h2>Level 2</h2>
        <Draggable
          onDragStart={(event) =>
            handleDragStart(event, level2Element.value, LEVEL_2_CONTRACT)
          }
        >
          <SpanStyled $isFaded={level < 2}>{level2Element.name}</SpanStyled>
        </Draggable>
      </Card>
    </>
  );
}

// ======================
//    # Data & Types #
// ======================

type ElementData = {
  id: string;
  value: string;
};

const LEVEL_1_CONTRACT = "level-1-link";
const LEVEL_2_CONTRACT = "level-2-link";

const elements = [
  {
    id: "7afad19c",
    name: "Element #1",
    value: "🥦",
  },
  {
    id: "e8dd6480",
    name: "Element #2",
    value: "💰",
  },
  {
    id: "081c67fc",
    name: "Element #3",
    value: "🪵",
  },
];

const level2Element = {
  id: "7795b2fa",
  name: "Element",
  value: "🚀",
};

// ===========================
//    # Styled Components #
// ===========================

const DroppedElement = styled.span`
  font-size: 4.2em;
`;

const DivStyled = styled.div`
  margin-top: 3.3em;
  text-transform: uppercase;
  font-weight: bold;
`;

const SpanStyled = styled.span<{ $isFaded: boolean }>`
  color: #ba5c12;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 0.9rem;
  opacity: ${(props) => (props.$isFaded ? "0.33" : "1")};
`;
