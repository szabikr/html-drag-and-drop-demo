import { DragEvent } from "react";
import styled from "styled-components";
import Card from "../components/Card";
import Dropzone from "../components/Dropzone";
import Draggable from "../components/Draggable";

export default function CopyOperation() {
  const handleDragStart = (event: DragEvent<HTMLDivElement>) => {
    event.dataTransfer.setData("text/plain", event.currentTarget.id);
  };

  const handleDrop = (event: DragEvent<HTMLDivElement>) => {
    const elementId = event.dataTransfer.getData("text/plain");
    const element = document.getElementById(elementId);
    if (!element) return;
    // TODO: Set this node to not be draggable anymore
    const clone = element.cloneNode(true);
    event.currentTarget.appendChild(clone);
  };

  return (
    <>
      <h1>Copy Operation</h1>
      <FlexBox>
        <Card>
          <h2>Draggable elements</h2>
          <Draggable id="element-id" onDragStart={handleDragStart}>
            <SpanStyled>Element</SpanStyled>
          </Draggable>
        </Card>

        <Card>
          <h2>Dropzone 2</h2>
          <Dropzone onDrop={handleDrop} />
        </Card>
      </FlexBox>
    </>
  );
}

// ===========================
//    # Styled Components #
// ===========================

const FlexBox = styled.div`
  display: flex;
  justify-content: space-around;
`;

const SpanStyled = styled.span`
  color: #ba5c12;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 0.9rem;
`;
